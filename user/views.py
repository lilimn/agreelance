from django.contrib.auth import authenticate
from django.shortcuts import render, redirect

from .forms import SignUpForm
from .models import UAdd


def index(request):
    return render(request, 'base.html')

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            create_new_user(form)
            from django.contrib import messages
            messages.success(request, 'Your account has been created and is awaiting verification.')
            return redirect('home')
    else:
        form = SignUpForm()
    return render(request, 'user/signup.html', {'form': form})

def create_new_user(form):
    user = form.save()

    user.refresh_from_db()

    user.profile.company = form.cleaned_data.get('company')
    print(form.cleaned_data.get('country'))
    user.profile.my_address = UAdd(country = form.cleaned_data.get('country'),
                                   state= form.cleaned_data.get('state'),
                                   city=form.cleaned_data.get('city'),
                                   postal_code=form.cleaned_data.get('postal_code'),
                                   street_address=form.cleaned_data.get('street_address'),
    )

    user.profile.my_address.save()

    user.is_active = False
    user.profile.categories.add(*form.cleaned_data['categories'])
    user.save()
    raw_password = form.cleaned_data.get('password1')
    user = authenticate(username=user.username, password=raw_password)
    return user
