
from django.test import Client
from django.test import RequestFactory, TestCase
from projects.models import ProjectCategory


# Create your tests here.

class exercise3_user_Refactor_Tests(TestCase):
    def test_user_signup_duplicate_code_extract_method(self):
        cleaning = ProjectCategory(name="Cleaning")
        cleaning.save()
        painting = ProjectCategory(name="Painting")
        painting.save()
        gardening = ProjectCategory(name="Gardening")
        gardening.save()

        client = Client()
        client.get('/user/signup/')
        response = client.post('/user/signup/', {'username':'john','password':'xhmbxmzxeigdxACGGGB@@@1677','first_name':'john','last_name':'mn','company':'ntnu',
                                                 'email':'lennon@thebeatles.com','email_confirmation':'lennon@thebeatles.com',
                                                 'password1':'xhmbxmzxeigdxACGGGB@@@1677',  'password2':'xhmbxmzxeigdxACGGGB@@@1677',
                                                 'phone_number':'12345678','country':'Norway','state':'T','city':'Trondheim','postal_code':'1234',
                                                 'street_address':'moholt','categories':1})
        self.assertEqual(response.status_code, 302)


