import datetime

from django.test import TestCase
from django.utils import timezone

from django.test import Client
from django.urls import reverse
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate
from user.models import Profile
from .models import Project, Task, TaskFile, TaskOffer, Delivery, ProjectCategory, Team, TaskFileTeam, directory_path, \
    SortOption
from .forms import ProjectForm, TaskFileForm, ProjectStatusForm, TaskOfferForm, TaskOfferResponseForm, TaskPermissionForm, DeliveryForm, TaskDeliveryResponseForm, TeamForm, TeamAddForm
from django.test import RequestFactory, TestCase
from .views import project_view, get_user_task_permissions, task_view, upload_file_to_task
from django.core.files.uploadedfile import File, SimpleUploadedFile


class Task2Test1(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
            username='ali', email='lennon@thebeatles.com', password='alipassword')
        self.profile = self.user.profile
        self.profile.save()

    def test_project_view_first_scenario(self):
        """
        automated test script to get a full statement coverage
        :return:
        """
        print("Task2Test1 First Test Scenario: False (if request.user == project.user.user:) and True (if request.method == 'POST' and 'offer_submit' in request.POST:) and  True (if task_offer_form.is_valid():)")
        cleaning = ProjectCategory(name="Cleaning")
        cleaning.save()
        painting = ProjectCategory(name="Painting")
        painting.save()
        gardening = ProjectCategory(name="Gardening")
        gardening.save()

        user01 = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        profile01 = user01.profile
        profile01.categories.add(cleaning)
        user01.save()

        date_posted = datetime.date.today
        deadline = datetime.date(2020, 4, 24)

        OPEN = 'o'

        project01 = Project(
            user = profile01,
            title = "prg01",
            description = "dsc01",
            category = cleaning,
            deadline = deadline,
            date_posted = date_posted,
            status = OPEN
        )
        project01.save()

        PENDING_ACCEPTANCE = 'pa'

        task01 = Task(
            project = project01,
            title = "tsk01",
            description = "tsk01dsc01",
            budget = 10,
            task_offerer = "first_user",
            status = PENDING_ACCEPTANCE
        )
        task01.save()

        taskoffer01 = TaskOffer(
            task = task01,
            title="mytaskoffer",
            description = "mytaskdescription",
            price = 10,
            offerer = profile01
        )
        taskoffer01.save()

        data = {'offer_submit': '','taskvalue':1, 'title': 'mytaskoffer', 'description': 'mytaskdescription', 'price':10 }

        request = self.factory.post('/projects/1/',data=data)
        request.user = self.user

        response = project_view(request,1)
        self.assertEqual(response.status_code, 200)

    def test_project_view_second_scenario(self):
        """
        automated test script to get a full statement coverage
        :return:
        """
        print(
            "Task2Test1 Second Test Scenario: True(if request.user == project.user.user:) and "
            "True (if request.method == 'POST' and 'offer_response' in request.POST:) and True (if offer_response_form.is_valid():) ")

        cleaning = ProjectCategory(name="Cleaning")
        cleaning.save()
        painting = ProjectCategory(name="Painting")
        painting.save()
        gardening = ProjectCategory(name="Gardening")
        gardening.save()

        user01 = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        profile01 = user01.profile
        profile01.categories.add(cleaning)
        user01.save()

        user02 = User.objects.create_user('jack', 'lennon@thebeatles.com', 'jackpassword')
        profile02 = user02.profile
        profile02.categories.add(cleaning)
        user02.save()


        date_posted = datetime.date.today
        deadline = datetime.date(2020, 4, 24)

        OPEN = 'o'
        INPROG = 'i'
        FINISHED = 'f'
        STATUS_CHOICES = (
                (OPEN, 'Open'),
                (INPROG, 'In progress'),
                (FINISHED, 'Finished'),
            )
        project01 = Project(
            user = self.profile,
            title = "prg01",
            description = "dsc01",
            category = cleaning,
            deadline = deadline,
            date_posted = date_posted,
            status = OPEN
        )
        project01.save()

        AWAITING_DELIVERY = 'ad'
        PENDING_ACCEPTANCE = 'pa'
        PENDING_PAYMENT = 'pp'
        PAYMENT_SENT = 'ps'
        DECLINED_DELIVERY = 'dd'
        STATUS_CHOICES = (
                (AWAITING_DELIVERY, 'Waiting for delivery'),
                (PENDING_ACCEPTANCE, 'Delivered and waiting for acceptance'),
                (PENDING_PAYMENT, 'Delivery has been accepted, awaiting payment'),
                (PAYMENT_SENT, 'Payment for delivery is done'),
                (DECLINED_DELIVERY, 'Declined delivery, please revise'),
            )


        task01 = Task(
            project = project01,
            title = "tsk01",
            description = "tsk01dsc01",
            budget = 10,
            task_offerer = "first_user",
            status = PENDING_ACCEPTANCE
        )
        task01.save()

        ACCEPTED = 'a'
        PENDING = 'p'
        DECLINED = 'd'

        taskoffer01 = TaskOffer(
            task=task01,
            title="mytaskoffer",
            description="mytaskdescription",
            price=10,
            offerer=profile02,
            status='a',
            feedback='the owner feedback'
        )

        taskoffer01.save()

        data = {'offer_response': '','taskofferid':1, 'title': 'mytaskoffer', 'description': 'mytaskdescription', 'price':10 , 'feedback':'the owner feedback',
                'status':'a'}


        request = self.factory.post('/projects/1/',data=data)
        request.user = self.user

        response = project_view(request,1)
        self.assertEqual(response.status_code, 200)

    def test_project_view_third_scenario(self):
        """
        automated test script to get a full statement coverage
        :return:
        """
        print(
            "Task2Test1 Third Test Scenario: True(if request.user == project.user.user:) and "
            "True (if request.method == 'POST' and 'status_change' in request.POST:) and True (if status_form.is_valid():)")

        cleaning = ProjectCategory(name="Cleaning")
        cleaning.save()
        painting = ProjectCategory(name="Painting")
        painting.save()
        gardening = ProjectCategory(name="Gardening")
        gardening.save()

        user01 = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        profile01 = user01.profile
        profile01.categories.add(cleaning)
        user01.save()

        user02 = User.objects.create_user('jack', 'lennon@thebeatles.com', 'jackpassword')
        profile02 = user02.profile
        profile02.categories.add(cleaning)
        user02.save()


        date_posted = datetime.date.today
        deadline = datetime.date(2020, 4, 24)

        OPEN = 'o'
        INPROG = 'i'
        FINISHED = 'f'
        STATUS_CHOICES = (
                (OPEN, 'Open'),
                (INPROG, 'In progress'),
                (FINISHED, 'Finished'),
            )
        project01 = Project(
            user = self.profile,
            title = "prg01",
            description = "dsc01",
            category = cleaning,
            deadline = deadline,
            date_posted = date_posted,
            status = 'f'
        )
        project01.save()

        AWAITING_DELIVERY = 'ad'
        PENDING_ACCEPTANCE = 'pa'
        PENDING_PAYMENT = 'pp'
        PAYMENT_SENT = 'ps'
        DECLINED_DELIVERY = 'dd'
        STATUS_CHOICES = (
                (AWAITING_DELIVERY, 'Waiting for delivery'),
                (PENDING_ACCEPTANCE, 'Delivered and waiting for acceptance'),
                (PENDING_PAYMENT, 'Delivery has been accepted, awaiting payment'),
                (PAYMENT_SENT, 'Payment for delivery is done'),
                (DECLINED_DELIVERY, 'Declined delivery, please revise'),
            )


        task01 = Task(
            project = project01,
            title = "tsk01",
            description = "tsk01dsc01",
            budget = 10,
            task_offerer = "first_user",
            status = PENDING_ACCEPTANCE
        )
        task01.save()

        ACCEPTED = 'a'
        PENDING = 'p'
        DECLINED = 'd'

        taskoffer01 = TaskOffer(
            task=task01,
            title="mytaskoffer",
            description="mytaskdescription",
            price=10,
            offerer=profile02,
            status='a',
            feedback='the owner feedback'
        )

        taskoffer01.save()

        data = {'status_change': '','status':'f'}

        request = self.factory.post('/projects/1/',data=data)
        request.user = self.user

        response = project_view(request,1)
        self.assertEqual(response.status_code, 200)


class Task2Test2(TestCase):

    def ttest_get_user_task_permissions_First_Test_Scenario(self):
        """
        automated test script to get a full statement coverage
        :return:
        """
        print("Task2Test2 First Test Scenario :True (if user == task.project.user.user)")

        factory = RequestFactory()
        user = User.objects.create_user(
            username='ali', email='lennon@thebeatles.com', password='alipassword')
        profile = user.profile
        cleaning = ProjectCategory(name="Cleaning")
        cleaning.save()
        painting = ProjectCategory(name="Painting")
        painting.save()
        gardening = ProjectCategory(name="Gardening")
        gardening.save()

        user01 = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        profile01 = user01.profile
        profile01.categories.add(cleaning)
        user01.save()

        date_posted = datetime.date.today
        deadline = datetime.date(2020, 4, 24)

        OPEN = 'o'
        INPROG = 'i'
        FINISHED = 'f'

        project01 = Project(
            user=profile01,
            title="prg01",
            description="dsc01",
            category=cleaning,
            deadline=deadline,
            date_posted=date_posted,
            status=OPEN
        )
        project01.save()

        AWAITING_DELIVERY = 'ad'
        PENDING_ACCEPTANCE = 'pa'
        PENDING_PAYMENT = 'pp'
        PAYMENT_SENT = 'ps'
        DECLINED_DELIVERY = 'dd'


        task01 = Task(
            project=project01,
            title="tsk01",
            description="tsk01dsc01",
            budget=10,
            task_offerer="first_user",
            status=PENDING_ACCEPTANCE
        )
        task01.save()

        taskoffer01 = TaskOffer(
            task=task01,
            title="mytaskoffer",
            description="mytaskdescription",
            price=10,
            offerer=profile01
        )
        taskoffer01.save()

        response = get_user_task_permissions(user01, task01)
        a = {'write': True, 'read': True, 'modify': True, 'owner': True, 'upload': True}
        self.assertEqual(response.items(),a.items())

    def ttest_get_user_task_permissions_second_test_scenario(self):
        """
        automated test script to get a full statement coverage
        :return:
        """
        print("Task2Test2 Second Test Scenario :True( if task.accepted_task_offer() and task.accepted_task_offer().offerer == user.profile)")

        factory = RequestFactory()
        user = User.objects.create_user(
            username='ali', email='lennon@thebeatles.com', password='alipassword')
        profile = user.profile
        cleaning = ProjectCategory(name="Cleaning")
        cleaning.save()
        painting = ProjectCategory(name="Painting")
        painting.save()
        gardening = ProjectCategory(name="Gardening")
        gardening.save()


        user01 = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        profile01 = user01.profile
        profile01.categories.add(cleaning)
        user01.save()

        date_posted = datetime.date.today
        deadline = datetime.date(2020, 4, 24)

        OPEN = 'o'
        INPROG = 'i'
        FINISHED = 'f'
        STATUS_CHOICES = (
            (OPEN, 'Open'),
            (INPROG, 'In progress'),
            (FINISHED, 'Finished'),
        )
        project01 = Project(
            user=profile01,
            title="prg01",
            description="dsc01",
            category=cleaning,
            deadline=deadline,
            date_posted=date_posted,
            status=OPEN
        )
        project01.save()

        AWAITING_DELIVERY = 'ad'
        PENDING_ACCEPTANCE = 'pa'
        PENDING_PAYMENT = 'pp'
        PAYMENT_SENT = 'ps'
        DECLINED_DELIVERY = 'dd'


        task01 = Task(
            project=project01,
            title="tsk01",
            description="tsk01dsc01",
            budget=10,
            task_offerer="first_user",
            status=PENDING_ACCEPTANCE
        )
        task01.save()

        ACCEPTED = 'a'
        PENDING = 'p'
        DECLINED = 'd'

        taskoffer01 = TaskOffer(
            task=task01,
            title="mytaskoffer",
            description="mytaskdescription",
            price=10,
            offerer=profile,
            status = ACCEPTED
        )
        taskoffer01.save()

        response = get_user_task_permissions(user, task01)
        a = {'write': True, 'read': True, 'modify': True, 'owner':False,'upload':True}
        self.assertEqual(response.items(), a.items())

    def ttest_get_user_task_permissions_third_test_scenario(self):
        """
        automated test script to get a full statement coverage
        :return:
        """
        print(
            "Task2Test2 Third Test scenario :True (if user == task.project.user.user $$ if task.accepted_task_offer() and task.accepted_task_offer().offerer == user.profile)")

        factory = RequestFactory()
        user = User.objects.create_user(
            username='ali', email='lennon@thebeatles.com', password='alipassword')
        profile = user.profile
        cleaning = ProjectCategory(name="Cleaning")
        cleaning.save()
        painting = ProjectCategory(name="Painting")
        painting.save()
        gardening = ProjectCategory(name="Gardening")
        gardening.save()

        user01 = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        profile01 = user01.profile
        profile01.categories.add(cleaning)
        user01.save()

        user02 = User.objects.create_user('jack', 'lennon@thebeatles.com', 'jackpassword')
        profile02 = user02.profile
        profile02.categories.add(cleaning)
        user02.save()

        date_posted = datetime.date.today
        deadline = datetime.date(2020, 4, 24)

        OPEN = 'o'
        INPROG = 'i'
        FINISHED = 'f'
        STATUS_CHOICES = (
            (OPEN, 'Open'),
            (INPROG, 'In progress'),
            (FINISHED, 'Finished'),
        )
        project01 = Project(
            user=profile01,
            title="prg01",
            description="dsc01",
            category=cleaning,
            deadline=deadline,
            date_posted=date_posted,
            status=OPEN
        )
        project01.save()

        AWAITING_DELIVERY = 'ad'
        PENDING_ACCEPTANCE = 'pa'
        PENDING_PAYMENT = 'pp'
        PAYMENT_SENT = 'ps'
        DECLINED_DELIVERY = 'dd'

        task01 = Task(
            project=project01,
            title="tsk01",
            description="tsk01dsc01",
            budget=10,
            task_offerer="first_user",
            status=PENDING_ACCEPTANCE
        )
        task01.save()

        ACCEPTED = 'a'
        PENDING = 'p'
        DECLINED = 'd'

        taskoffer01 = TaskOffer(
            task=task01,
            title="mytaskoffer",
            description="mytaskdescription",
            price=10,
            offerer=profile02,
            status=ACCEPTED
        )
        taskoffer01.save()

        response = get_user_task_permissions(user, task01)
        a = {'write': False, 'read': False, 'modify': False, 'owner': False, 'view_task':False,'upload': False}
        self.assertEqual(response.items(), a.items())

class exercise3_projects_Refactor_Tests(TestCase):
    def test_projects_duplicate_code_substitude_algorithm(self):
        client = Client()
        response = client.post('/projects/', {'username':'momen','password':'momen','sopts':'title'})
        self.assertEqual(response.status_code, 200)

    def test_new_project_duplicate_code_extract_method(self):
        cleaning = ProjectCategory(name="Cleaning")
        cleaning.save()
        painting = ProjectCategory(name="Painting")
        painting.save()
        gardening = ProjectCategory(name="Gardening")
        gardening.save()

        user01 = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        profile01 = user01.profile
        profile01.categories.add(cleaning)
        user01.save()


        client = Client()
        client.force_login(user01)
        client.get('/projects/new/')
        response = client.post('/projects/new/', {
                                                  'title':'a', 'description':'a','category_id':'1','deadline':datetime.date(2021, 4, 24),
                                                  'task_title':'ta','task_description':'ta','task_budget':10})


        self.assertEqual(response.status_code, 302)


    def test_task_view_long_method_extract_method01(self):
        """
        automated test script to get a full statement coverage
        :return:
        """
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
            username='ali', email='lennon@thebeatles.com', password='alipassword')
        self.profile = self.user.profile
        self.profile.save()

        print("True (if request.method == 'POST' and 'delivery' in request.POST:) ")
        cleaning = ProjectCategory(name="Cleaning")
        cleaning.save()
        painting = ProjectCategory(name="Painting")
        painting.save()
        gardening = ProjectCategory(name="Gardening")
        gardening.save()

        user01 = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        profile01 = user01.profile
        profile01.categories.add(cleaning)
        user01.save()

        date_posted = datetime.date.today
        deadline = datetime.date(2021, 4, 24)

        OPEN = 'o'

        project01 = Project(
            user=profile01,
            title="prg01",
            description="dsc01",
            category=cleaning,
            deadline=deadline,
            date_posted=date_posted,
            status=OPEN
        )
        project01.save()

        PENDING_ACCEPTANCE = 'pa'

        task01 = Task(
            project=project01,
            title="tsk01",
            description="tsk01dsc01",
            budget=10,
            task_offerer="first_user",
            status=PENDING_ACCEPTANCE
        )
        task01.save()

        taskoffer01 = TaskOffer(
            task=task01,
            title="mytaskoffer",
            description="mytaskdescription",
            price=10,
            offerer=self.profile,
            status='a',
            feedback='the owner feedback'
        )
        taskoffer01.save()

        data = {'delivery': '','comment':'this is the first time'}
        with open('fp.doc', 'rb') as fp:
            request = self.factory.post('projects/1/tasks/1/', data=data)
            request.FILES['file'] = fp
            request.FILES['file'].read()
        request.user = self.user

        response = task_view(request, 1,1)
        self.assertEqual(response.status_code, 200)

    def test_task_view_long_method_extract_method02(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username='ali', email='lennon@thebeatles.com', password='alipassword')
        self.profile = self.user.profile
        self.profile.save()
        print("True ( if request.method == 'POST' and 'delivery-response' in request.POST:) ")
        cleaning = ProjectCategory(name="Cleaning")
        cleaning.save()
        user01 = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        profile01 = user01.profile
        profile01.categories.add(cleaning)
        user01.save()
        date_posted = datetime.date.today
        deadline = datetime.date(2021, 4, 24)
        OPEN = 'o'
        project01 = Project(user=profile01,title="prg01",description="dsc01",category=cleaning,deadline=deadline,date_posted=date_posted,status=OPEN )
        project01.save()
        PENDING_ACCEPTANCE = 'pa'
        task01 = Task(project=project01,title="tsk01",description="tsk01dsc01",budget=10,task_offerer="first_user",status=PENDING_ACCEPTANCE)
        task01.save()
        taskoffer01 = TaskOffer(task=task01,title="mytaskoffer",description="mytaskdescription",price=10,offerer=self.profile,status='a',feedback='the owner feedback' )
        taskoffer01.save()
        delivery01 = Delivery(task=task01,file = 'Try.txt', delivery_user = profile01, delivery_time = datetime.date.today, comment ="this is my delivery",status = 'a', feedback = "this is my feedback")
        delivery01.save()
        data = {'delivery-response': '','delivery-id':1, 'comment':'this is my comment'}
        with open('fp.doc', 'rb') as fp:
            request = self.factory.post('projects/1/tasks/1/', data=data)
            request.FILES['file'] = fp
            request.FILES['file'].read()
        request.user = self.user
        response = task_view(request, 1,1)
        self.assertEqual(response.status_code, 200)

    def test_task_view_long_method_extract_method03(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username='ali', email='lennon@thebeatles.com', password='alipassword')
        self.profile = self.user.profile
        self.profile.save()
        print("True ( if request.method == 'POST' and 'team' in request.POST:) ")
        cleaning = ProjectCategory(name="Cleaning")
        cleaning.save()
        user01 = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        profile01 = user01.profile
        profile01.categories.add(cleaning)
        user01.save()
        date_posted = datetime.date.today
        deadline = datetime.date(2021, 4, 24)
        OPEN = 'o'
        project01 = Project(user=profile01,title="prg01",description="dsc01",category=cleaning,deadline=deadline,date_posted=date_posted,status=OPEN )
        project01.save()
        PENDING_ACCEPTANCE = 'pa'
        task01 = Task(project=project01,title="tsk01",description="tsk01dsc01",budget=10,task_offerer="first_user",status=PENDING_ACCEPTANCE)
        task01.save()
        taskoffer01 = TaskOffer(task=task01,title="mytaskoffer",description="mytaskdescription",price=10,offerer=self.profile,status='a',feedback='the owner feedback' )
        taskoffer01.save()
        delivery01 = Delivery(task=task01,file = 'Try.txt', delivery_user = profile01, delivery_time = datetime.date.today, comment ="this is my delivery",status = 'a', feedback = "this is my feedback")
        delivery01.save()
        data = {'team': '','delivery-id':1, 'name':'our team'}
        request = self.factory.post('projects/1/tasks/1/', data=data)
        request.user = self.user
        response = task_view(request, 1,1)
        self.assertEqual(response.status_code, 200)

    def test_task_view_long_method_extract_method04(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username='ali', email='lennon@thebeatles.com', password='alipassword')
        self.profile = self.user.profile
        self.profile.save()
        print("True ( if request.method == 'POST' and 'team-add' in request.POST:) ")
        cleaning = ProjectCategory(name="Cleaning")
        cleaning.save()
        user01 = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        profile01 = user01.profile
        profile01.categories.add(cleaning)
        user01.save()
        date_posted = datetime.date.today
        deadline = datetime.date(2021, 4, 24)
        OPEN = 'o'
        project01 = Project(user=profile01,title="prg01",description="dsc01",category=cleaning,deadline=deadline,date_posted=date_posted,status=OPEN )
        project01.save()
        PENDING_ACCEPTANCE = 'pa'
        task01 = Task(project=project01,title="tsk01",description="tsk01dsc01",budget=10,task_offerer="first_user",status=PENDING_ACCEPTANCE)
        task01.save()
        taskoffer01 = TaskOffer(task=task01,title="mytaskoffer",description="mytaskdescription",price=10,offerer=self.profile,status='a',feedback='the owner feedback' )
        taskoffer01.save()
        delivery01 = Delivery(task=task01,file = 'Try.txt', delivery_user = profile01, delivery_time = datetime.date.today, comment ="this is my delivery",status = 'a', feedback = "this is my feedback")
        delivery01.save()
        team01 =Team(name="our team",task=task01)
        team01.save()
        team01.members.add(profile01,self.profile)

        data = {'team-add': '','team-id':1, 'members':{1,2}}
        request = self.factory.post('projects/1/tasks/1/', data=data)
        request.user = self.user
        response = task_view(request, 1,1)
        self.assertEqual(response.status_code, 200)

    def test_task_view_long_method_extract_method05(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username='ali', email='lennon@thebeatles.com', password='alipassword')
        self.profile = self.user.profile
        self.profile.save()
        print("True ( if request.method == 'POST' and 'permissions' in request.POST:) ")
        cleaning = ProjectCategory(name="Cleaning")
        cleaning.save()
        user01 = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        profile01 = user01.profile
        profile01.categories.add(cleaning)
        user01.save()
        date_posted = datetime.date.today
        deadline = datetime.date(2021, 4, 24)
        OPEN = 'o'
        project01 = Project(user=profile01,title="prg01",description="dsc01",category=cleaning,deadline=deadline,date_posted=date_posted,status=OPEN )
        project01.save()
        PENDING_ACCEPTANCE = 'pa'
        task01 = Task(project=project01,title="tsk01",description="tsk01dsc01",budget=10,task_offerer="first_user",status=PENDING_ACCEPTANCE)
        task01.save()
        taskfile01 = TaskFile(task = task01, file = 'Try.txt')
        taskfile01.save()
        taskoffer01 = TaskOffer(task=task01,title="mytaskoffer",description="mytaskdescription",price=10,offerer=self.profile,status='a',feedback='the owner feedback' )
        taskoffer01.save()
        delivery01 = Delivery(task=task01,file = 'Try.txt', delivery_user = profile01, delivery_time = datetime.date.today, comment ="this is my delivery",status = 'a', feedback = "this is my feedback")
        delivery01.save()
        team01 =Team(name="our team",task=task01)
        team01.save()
        team01.members.add(profile01,self.profile)
        taskfileteam01 = TaskFileTeam(file = taskfile01,team = team01, name="nn", read=False,write=False,modify=False)
        taskfileteam01.save()

        data = {'permissions': '','team-id':1, 'permission-read-1-1':True,'permission-write-1-1':True, 'permission-modify-1-1':True, 'permission-perobj-1-1':1}
        request = self.factory.post('projects/1/tasks/1/', data=data)
        request.user = self.user
        response = task_view(request, 1,1)
        self.assertEqual(response.status_code, 200)

    def test_task_view_long_method_extract_method06(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username='ali', email='lennon@thebeatles.com', password='alipassword')
        self.profile = self.user.profile
        self.profile.save()
        print("True ( if user_permissions['read'] or user_permissions['write'] or user_permissions['modify'] or user_permissions['owner'] or user_permissions['view_task']:) ")
        cleaning = ProjectCategory(name="Cleaning")
        cleaning.save()
        user01 = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        profile01 = user01.profile
        profile01.categories.add(cleaning)
        user01.save()
        date_posted = datetime.date.today
        deadline = datetime.date(2021, 4, 24)
        OPEN = 'o'
        project01 = Project(user=profile01,title="prg01",description="dsc01",category=cleaning,deadline=deadline,date_posted=date_posted,status=OPEN )
        project01.save()
        PENDING_ACCEPTANCE = 'pa'
        task01 = Task(project=project01,title="tsk01",description="tsk01dsc01",budget=10,task_offerer="first_user",status=PENDING_ACCEPTANCE)
        task01.save()
        taskfile01 = TaskFile(task = task01, file = 'Try.txt')
        taskfile01.save()
        taskoffer01 = TaskOffer(task=task01,title="mytaskoffer",description="mytaskdescription",price=10,offerer=self.profile,status='a',feedback='the owner feedback' )
        taskoffer01.save()
        delivery01 = Delivery(task=task01,file = 'Try.txt', delivery_user = profile01, delivery_time = datetime.date.today, comment ="this is my delivery",status = 'a', feedback = "this is my feedback")
        delivery01.save()
        team01 =Team(name="our team",task=task01)
        team01.save()
        team01.members.add(profile01,self.profile)
        taskfileteam01 = TaskFileTeam(file = taskfile01,team = team01, name="nn", read=False,write=False,modify=False)
        taskfileteam01.save()

        data = {'permissions': '','team-id':1, 'permission-read-1-1':True,'permission-write-1-1':True, 'permission-modify-1-1':True, 'permission-perobj-1-1':1}
        request = self.factory.post('projects/1/tasks/1/', data=data)
        request.user = self.user
        response = task_view(request, 1,1)
        self.assertEqual(response.status_code, 200)

    def test_upload_file_to_task_long_method_extract_method06(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username='ali', email='lennon@thebeatles.com', password='alipassword')
        self.profile = self.user.profile
        self.profile.save()
        print(
            "True (if user_permissions['modify'] or user_permissions['write'] or user_permissions['upload'] or isProjectOwner(request.user, project):) ")
        cleaning = ProjectCategory(name="Cleaning")
        cleaning.save()
        user01 = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        profile01 = user01.profile
        profile01.categories.add(cleaning)
        user01.save()
        date_posted = datetime.date.today
        deadline = datetime.date(2021, 4, 24)
        OPEN = 'o'
        project01 = Project(user=profile01, title="prg01", description="dsc01", category=cleaning,
                            deadline=deadline, date_posted=date_posted, status=OPEN)
        project01.save()
        PENDING_ACCEPTANCE = 'pa'
        task01 = Task(project=project01, title="tsk01", description="tsk01dsc01", budget=10,
                      task_offerer="first_user", status=PENDING_ACCEPTANCE)
        task01.save()
        taskfile01 = TaskFile(task=task01, file='Try.txt')
        taskfile01.save()
        taskoffer01 = TaskOffer(task=task01, title="mytaskoffer", description="mytaskdescription", price=10,
                                offerer=self.profile, status='a', feedback='the owner feedback')
        taskoffer01.save()
        delivery01 = Delivery(task=task01, file='Try.txt', delivery_user=profile01,
                              delivery_time=datetime.date.today, comment="this is my delivery", status='a',
                              feedback="this is my feedback")
        delivery01.save()
        team01 = Team(name="our team", task=task01)
        team01.save()
        team01.members.add(profile01, self.profile)
        taskfileteam01 = TaskFileTeam(file=taskfile01, team=team01, name="nn", read=False, write=False,
                                      modify=False)
        taskfileteam01.save()


        with open('fp.doc', 'rb') as fp:
            data = {'task':1}
            request = self.factory.post('projects/1/tasks/1/upload/')
            request.FILES['file'] = fp
            request.FILES['id'] = 1
            request.FILES['file'].read()
        request.user = user01
        response = upload_file_to_task(request, 1, 1)
        self.assertEqual(response.status_code, 200)
